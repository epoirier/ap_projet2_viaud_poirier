package com.example.ap_projet_resto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.text.HtmlCompat;

import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class activity_detailsResto extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_resto);

        // Récupération des textView où l'on placera les informations du restaurant
        TextView nomTextView = findViewById(R.id.page_detailsResto_textView_NomResto);
        TextView descriptionTextView = findViewById(R.id.page_detailsResto_textView_descriptionResto);
        TextView adresseTextView = findViewById(R.id.page_detailsResto_textView_adresseResto);
        TextView horaireSemaineMidi = findViewById(R.id.page_detailsResto_tableRow_SemaineMidi);
        TextView horaireWeekendMidi = findViewById(R.id.page_detailsResto_tableRow_WeekendMidi);
        TextView horaireSemaineSoir = findViewById(R.id.page_detailsResto_tableRow_SemaineSoir);
        TextView horaireWeekendSoir = findViewById(R.id.page_detailsResto_tableRow_WeekendSoir);
        TextView horaireSemaineEmporter = findViewById(R.id.page_detailsResto_tableRow_SemaineEmporter);
        TextView horaireWeekendEmporter = findViewById(R.id.page_detailsResto_tableRow_WeekendEmporter);
        ImageView logoImageView = findViewById(R.id.page_detailsResto_imageView_imageResto);

        // Récupération des détails du restaurants transmis par le intent
        Intent detailsResto = getIntent();
        String nomResto = detailsResto.getStringExtra("nomResto");
        String horaireResto = detailsResto.getStringExtra("horaireResto");
        String descriptionResto = detailsResto.getStringExtra("descriptionResto");
        String adresseResto = detailsResto.getStringExtra("adresseResto");
        String lienImageResto = detailsResto.getStringExtra("imageResto");


        // Assignation des informations à notre détails du restaurant

        List<String> leshoraires = new ArrayList<>();
        // Chargement du document HTML dans un objet Document
        Document doc = Jsoup.parse(horaireResto);

        // Sélection de toutes les balises <td> du document
        Elements tdElements = (Elements) doc.getElementsByTag("td");

        // Parcours de toutes les balises <td> et ajout de leur contenu à la liste
        for (Element tdElement : tdElements) {
            leshoraires.add(tdElement.text());
        }
        // Affichage de notre liste
        Log.d("DEBUG", String.valueOf(leshoraires));

        nomTextView.setText(nomResto);
        descriptionTextView.setText(descriptionResto);
        descriptionTextView.setMovementMethod(new ScrollingMovementMethod());
        adresseTextView.setText(adresseResto);
        horaireSemaineMidi.setText(leshoraires.get(1));
        horaireWeekendMidi.setText(leshoraires.get(2));
        horaireSemaineSoir.setText(leshoraires.get(4));
        horaireWeekendSoir.setText(leshoraires.get(5));
        horaireSemaineEmporter.setText(leshoraires.get(7));
        horaireWeekendEmporter.setText(leshoraires.get(8));
        new DownloadImageTask(logoImageView).execute("http://10.0.2.2/API/Images/" + lienImageResto);

        // Affichage de nos horaires dans les textViews




    }
}