package com.example.ap_projet_resto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class activity_connexion extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        Button buttonConnexion = findViewById(R.id.page_accueil_buttonConnexion);


        // ------- CONNEXION A LA BASE DE DONNEES -------

        Request requestClients = new Request.Builder().url("http://10.0.2.2/API/BDD/ConnexionBDD.php").build();
        //exécution de cette requête
        OkHttpClient httpclient = new OkHttpClient();
        httpclient.newCall(requestClients).enqueue(new Callback() {
        //cette requête s’exécutera de façon asynchrone si la requête échoue affichage d'un message d'erreur dans les log
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                Log.i("Erreur", "Erreur requête ConnexionBDD.php");
            }
            //si la requête réussie
            public void onResponse(Call call, Response response) throws IOException {
                //on va traiter la réponse
                final String myResponse = response.body().string();
                Log.d("Connexion", "La connexion à la base de donnée à été réussie");
                }
        });

        // ------- FIN DE LA CONNEXION A LA BASE DE DONNEES -------

        buttonConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent versListeResto = new Intent(activity_connexion.this,activity_listeresto.class);
                startActivity(versListeResto);
            }
        });

    }
}