package com.example.ap_projet_resto.metier;

public class Photo {
    private int idP;
    private String cheminP;

    public Photo(int idP, String cheminP) {
        this.idP = idP;
        this.cheminP = cheminP;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public String getCheminP() {
        return cheminP;
    }

    public void setCheminP(String cheminP) {
        this.cheminP = cheminP;
    }
}

