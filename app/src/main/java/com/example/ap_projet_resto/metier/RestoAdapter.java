package com.example.ap_projet_resto.metier;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ap_projet_resto.DownloadImageTask;
import com.example.ap_projet_resto.R;
import com.example.ap_projet_resto.activity_detailsResto;

import java.util.List;

public class RestoAdapter extends BaseAdapter {

    private Context contexte;
    private List<Resto> lesRestos;
    private LayoutInflater inflater;

    public RestoAdapter(Context contexte, List<Resto> lesRestos) {
        this.contexte = contexte;
        this.lesRestos = lesRestos;
        this.inflater = LayoutInflater.from(contexte);
    }

    @Override
    public int getCount() {
        return lesRestos.size();
    }

    @Override
    public Resto getItem(int position) {
        return lesRestos.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflater.inflate(R.layout.adapter_item,null);
        // Récupère des informations à propos de l'item.
        Resto currentItem = getItem(i);
        String restoNom = currentItem.getNomResto();
        String restoHoraire = currentItem.getHoraireResto();
        String restoDescription = currentItem.getDescriptionResto();
        String restoAdresse = currentItem.getNumAdresseResto() + " " + currentItem.getVoieAdresseResto() + " " + currentItem.getVilleResto();

        TextView itemNomView = view.findViewById(R.id.adapter_item_NomResto);
        itemNomView.setText(restoNom);
        TextView itemAdresseView = view.findViewById(R.id.adapter_item_AdresseResto);
        itemAdresseView.setText(restoAdresse);


        ImageView imgResto = (ImageView) view.findViewById(R.id.adapter_item_imageResto);
        String lienImageResto = lesRestos.get(i).getLesPhotos().get(0).getCheminP();
        // téléchargement de l'image du restaurant
        new DownloadImageTask(imgResto).execute("http://10.0.2.2/API/Images/" + lesRestos.get(i).getLesPhotos().get(0).getCheminP());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent versDetailsResto = new Intent(contexte, activity_detailsResto.class);
                versDetailsResto.putExtra("nomResto",restoNom);
                versDetailsResto.putExtra("horaireResto",restoHoraire);
                versDetailsResto.putExtra("descriptionResto",restoDescription);
                versDetailsResto.putExtra("adresseResto",restoAdresse);
                versDetailsResto.putExtra("imageResto",lienImageResto);

                contexte.startActivity(versDetailsResto);
            }
        });



        return view;
    }
}
