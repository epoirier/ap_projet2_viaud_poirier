package com.example.ap_projet_resto.metier;

import java.util.ArrayList;

public class Resto {

    private int idResto;
    private String nomResto;
    private String numAdresseResto;
    private String voieAdresseResto;
    private String codePostalResto;
    private String villeResto;
    private float latitudeResto;
    private float longitudeResto;
    private String descriptionResto;
    private String horaireResto;
    private ArrayList<Photo> lesPhotos;


    public Resto(int idResto, String nomResto, String numAdresseResto, String voieAdresseResto, String codePostalResto, String villeResto, float latitudeResto, float longitudeResto, String descriptionResto, String horaireResto) {
        this.idResto = idResto;
        this.nomResto = nomResto;
        this.numAdresseResto = numAdresseResto;
        this.voieAdresseResto = voieAdresseResto;
        this.codePostalResto = codePostalResto;
        this.villeResto = villeResto;
        this.latitudeResto = latitudeResto;
        this.longitudeResto = longitudeResto;
        this.descriptionResto = descriptionResto;
        this.horaireResto = horaireResto;
        this.lesPhotos = new ArrayList<Photo>();    }

    public ArrayList<Photo> getLesPhotos() {
        return lesPhotos;
    }

    public void setLesPhotos(ArrayList<Photo> lesPhotos) {
        this.lesPhotos = lesPhotos;
    }

    public int getIdResto() {
        return idResto;
    }

    public void setIdResto(int idResto) {
        this.idResto = idResto;
    }

    public String getNomResto() {
        return nomResto;
    }

    public void setNomResto(String nomResto) {
        this.nomResto = nomResto;
    }

    public String getNumAdresseResto() {
        return numAdresseResto;
    }

    public void setNumAdresseResto(String numAdresseResto) {
        this.numAdresseResto = numAdresseResto;
    }

    public String getVoieAdresseResto() {
        return voieAdresseResto;
    }

    public void setVoieAdresseResto(String voieAdresseResto) {
        this.voieAdresseResto = voieAdresseResto;
    }

    public String getCodePostalResto() {
        return codePostalResto;
    }

    public void setCodePostalResto(String codePostalResto) {
        this.codePostalResto = codePostalResto;
    }

    public String getVilleResto() {
        return villeResto;
    }

    public void setVilleResto(String villeResto) {
        this.villeResto = villeResto;
    }

    public float getLatitudeResto() {
        return latitudeResto;
    }

    public void setLatitudeResto(float latitudeResto) {
        this.latitudeResto = latitudeResto;
    }

    public float getLongitudeResto() {
        return longitudeResto;
    }

    public void setLongitudeResto(float longitudeResto) {
        this.longitudeResto = longitudeResto;
    }

    public String getDescriptionResto() {
        return descriptionResto;
    }

    public void setDescriptionResto(String descriptionResto) {
        this.descriptionResto = descriptionResto;
    }

    public String getHoraireResto() {
        return horaireResto;
    }

    public void setHoraireResto(String horaireResto) {
        this.horaireResto = horaireResto;
    }

    @Override
    public String toString() {
        return nomResto + '\'' +" " + villeResto;
    }
}
