package com.example.ap_projet_resto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ap_projet_resto.metier.Photo;
import com.example.ap_projet_resto.metier.Resto;
import com.example.ap_projet_resto.metier.RestoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class activity_listeresto extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listeresto);

        ImageButton buttonReload = findViewById(R.id.page_listeResto_imageButton_Reload);
        Button buttonDeconnexion = findViewById(R.id.page_listeresto_header_buttonDeconnexion);
        Button buttonRechercher = findViewById(R.id.page_listeresto_searchbar_buttonRechercher);
        EditText editTextRechercher = findViewById(R.id.page_listeresto_editText_navbar);
        GridView listViewLesRestaurants = findViewById(R.id.page_listeresto_listView);
        ArrayList<Resto> lesResto = new ArrayList<Resto>();

        // ------- LISTE DEROULANTE DE LA BASE DE DONNEES -------

        Request requestClients = new Request.Builder().url("http://10.0.2.2/API/Resto/GetAllResto.php").build();
        //exécution de cette requête
        OkHttpClient httpclient = new OkHttpClient();
        httpclient.newCall(requestClients).enqueue(new Callback() {
            //cette requête s’exécutera de façon asynchrone si la requête échoue affichage d'un message d'erreur dans les log
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                Log.i("Erreur", "erreur requête getAllLacsJSON.php");
            }
            //si la requête réussie
            public void onResponse(Call call, Response response) throws IOException {
                //on va traiter la réponse
                final String myResponse = response.body().string();
                Log.d("Connexion", "La connexion à la base de donnée à été réussie");
                activity_listeresto.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            // on crée un objet JSON à partir de notre réponse.
                            JSONObject jsonObjectlesResto = new JSONObject(myResponse);
                            //on transforme cet objet JSON en array d'objet lac sous forme JSON
                            JSONArray jsonArray = jsonObjectlesResto.optJSONArray("resto");
                            //on parcours cette collection d'objet lacs pour ajouter chaque lac dans notre liste d'objet lac
                            lesResto.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String idResto = jsonObject.getString("idR");
                                String nomResto = jsonObject.getString("nomR");
                                String numAdresseResto = jsonObject.getString("numAdrR");
                                String voieAdresseResto = jsonObject.getString("voieAdrR");
                                String codePostalResto = jsonObject.getString("cpR");
                                String villeResto = jsonObject.getString("villeR");
                                String latitudeResto = jsonObject.getString("latitudeDegR");
                                String longitudeResto = jsonObject.getString("longitudeDegR");
                                String descriptionResto = jsonObject.getString("descR");
                                String horaireResto = jsonObject.getString("horairesR");
                                ArrayList<Photo> lesPhotos = new ArrayList<Photo>();
                                JSONArray jsonPhotos = jsonObject.getJSONArray("photos");
                                for (int j = 0; j < jsonPhotos.length(); j++) {
                                    JSONObject jsonPhoto = jsonPhotos.getJSONObject(j);
                                    Photo photo = new Photo(jsonPhoto.getInt("idP"), jsonPhoto.get("cheminP").toString());
                                    lesPhotos.add(photo);
                                }

                                Resto unResto = new Resto(Integer.parseInt(idResto),nomResto,numAdresseResto,voieAdresseResto,codePostalResto,villeResto,Float.parseFloat(latitudeResto),Float.parseFloat(longitudeResto),descriptionResto,horaireResto);
                                unResto.setLesPhotos(lesPhotos);
                                //on ajoute le resto à la collection lesResto
                                lesResto.add(unResto);
                                ArrayAdapter<Resto> dataAdapter = new ArrayAdapter<Resto>(activity_listeresto.this, android.R.layout.simple_list_item_1, lesResto);
                                listViewLesRestaurants.setAdapter(new RestoAdapter(activity_listeresto.this,lesResto));

                            }

                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });






            }
        });

        // ------- FIN DE LA LISTE DEROULANTE DE LA BASE DE DONNEES -------
        // ---------------- RECHERCHE D'UN RESTAURANT -----------------

        buttonRechercher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nomRestoRechercher = editTextRechercher.getText().toString();

                // Crée un nouvel objet OkHttpClient pour gérer les connexions HTTP
                OkHttpClient client = new OkHttpClient();

                // Crée une URL pour mon fichier PHP
                String url = "http:/10.0.2.2/API/Resto/GetOneByNom.php";

                // Crée un nouvel objet RequestBody avec les paramètres de la requête
                RequestBody body = new FormBody.Builder()
                        .add("name", nomRestoRechercher) // remplace "Nom du restaurant" par la valeur que vous souhaitez envoyer
                        .build();

                // Crée une nouvelle requête POST avec l'objet RequestBody
                Request request = new Request.Builder().url(url).post(body).build();

                // Envoye la requête et traite la réponse du serveur
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                        Log.i("Erreur", "erreur requête getOneByNom.php");
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (response.isSuccessful()) {
                            // on crée un objet JSON à partir de notre réponse.
                            JSONObject jsonObjectleResto = null;
                            final String myResponse = response.body().string();
                            try {
                                jsonObjectleResto = new JSONObject(myResponse);
                                //on transforme cet objet JSON en array d'objet resto sous forme JSON
                                JSONArray jsonArray = jsonObjectleResto.optJSONArray("resto");

                                // Si la réponse à notre requête contient plusieurs résultat
                                // Alors on remplace la liste des restaurants par nos résultats trouvées
                                // Sinon on affiche directement la fiche du restaurant en détail.
                                if (jsonArray.length() > 1) {
                                    Log.d("DEBUG","Il y a plusieurs résultats dans le fichier JSON.");
                                    activity_listeresto.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                // on crée un objet JSON à partir de notre réponse.
                                                JSONObject jsonObjectlesResto = new JSONObject(myResponse);
                                                //on transforme cet objet JSON en array d'objet lac sous forme JSON
                                                JSONArray jsonArray = jsonObjectlesResto.optJSONArray("resto");
                                                //on parcours cette collection d'objet lacs pour ajouter chaque lac dans notre liste d'objet lac
                                                lesResto.clear();
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                                    String idResto = jsonObject.getString("idR");
                                                    String nomResto = jsonObject.getString("nomR");
                                                    String numAdresseResto = jsonObject.getString("numAdrR");
                                                    String voieAdresseResto = jsonObject.getString("voieAdrR");
                                                    String codePostalResto = jsonObject.getString("cpR");
                                                    String villeResto = jsonObject.getString("villeR");
                                                    String latitudeResto = jsonObject.getString("latitudeDegR");
                                                    String longitudeResto = jsonObject.getString("longitudeDegR");
                                                    String descriptionResto = jsonObject.getString("descR");
                                                    String horaireResto = jsonObject.getString("horairesR");
                                                    ArrayList<Photo> lesPhotos = new ArrayList<Photo>();
                                                    JSONArray jsonPhotos = jsonObject.getJSONArray("photos");
                                                    for (int j = 0; j < jsonPhotos.length(); j++) {
                                                        JSONObject jsonPhoto = jsonPhotos.getJSONObject(j);
                                                        Photo photo = new Photo(jsonPhoto.getInt("idP"), jsonPhoto.get("cheminP").toString());
                                                        lesPhotos.add(photo);
                                                    }

                                                    Resto unResto = new Resto(Integer.parseInt(idResto),nomResto,numAdresseResto,voieAdresseResto,codePostalResto,villeResto,Float.parseFloat(latitudeResto),Float.parseFloat(longitudeResto),descriptionResto,horaireResto);
                                                    unResto.setLesPhotos(lesPhotos);
                                                    //on ajoute le resto à la collection lesResto
                                                    lesResto.add(unResto);
                                                    ArrayAdapter<Resto> dataAdapter = new ArrayAdapter<Resto>(activity_listeresto.this, android.R.layout.simple_list_item_1, lesResto);
                                                    listViewLesRestaurants.setAdapter(new RestoAdapter(activity_listeresto.this,lesResto));

                                                }

                                            }catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                                else {
                                    Log.d("DEBUG","Il n'y a qu'un seul résultat dans le fichier JSON.");
                                    //on parcours cette collection d'objet resto pour ajouter chaque resto dans notre liste d'objet resto
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        String idResto = jsonObject.getString("idR");
                                        String nomResto = jsonObject.getString("nomR");
                                        String numAdresseResto = jsonObject.getString("numAdrR");
                                        String voieAdresseResto = jsonObject.getString("voieAdrR");
                                        String codePostalResto = jsonObject.getString("cpR");
                                        String villeResto = jsonObject.getString("villeR");
                                        String latitudeResto = jsonObject.getString("latitudeDegR");
                                        String longitudeResto = jsonObject.getString("longitudeDegR");
                                        String descriptionResto = jsonObject.getString("descR");
                                        String horaireResto = jsonObject.getString("horairesR");
                                        String restoAdresse = numAdresseResto + voieAdresseResto + codePostalResto + villeResto;
                                        ArrayList<Photo> lesPhotos = new ArrayList<Photo>();
                                        JSONArray jsonPhotos = jsonObject.getJSONArray("photos");
                                        String laPhoto = null;
                                        for (int j = 0; j < jsonPhotos.length(); j++) {
                                            JSONObject jsonPhoto = jsonPhotos.getJSONObject(j);
                                            Photo photo = new Photo(jsonPhoto.getInt("idP"), jsonPhoto.get("cheminP").toString());
                                            laPhoto = jsonPhoto.get("cheminP").toString();
                                            lesPhotos.add(photo);
                                        }

                                        Resto leResto = new Resto(Integer.parseInt(idResto), nomResto, numAdresseResto, voieAdresseResto, codePostalResto, villeResto, Float.parseFloat(latitudeResto), Float.parseFloat(longitudeResto), descriptionResto, horaireResto);
                                        leResto.setLesPhotos(lesPhotos);

                                        Intent versDetailsResto = new Intent(activity_listeresto.this, activity_detailsResto.class);
                                        versDetailsResto.putExtra("nomResto", leResto.getNomResto());
                                        versDetailsResto.putExtra("horaireResto", leResto.getHoraireResto());
                                        versDetailsResto.putExtra("descriptionResto", leResto.getDescriptionResto());
                                        versDetailsResto.putExtra("adresseResto", leResto.getNumAdresseResto() + " " + leResto.getVoieAdresseResto() + " " + leResto.getCodePostalResto() + " " + leResto.getVilleResto());
                                        versDetailsResto.putExtra("imageResto", laPhoto);

                                        startActivity(versDetailsResto);
                                }


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(activity_listeresto.this, "Aucun restaurant de ce nom n'a été trouvé.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });            }
        });


        // -------------- RECHARGEMENT DE LA LISTE SI LE BOUTON RELOAD EST PRESSE -----------------------

        buttonReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Request requestClients = new Request.Builder().url("http://10.0.2.2/API/Resto/GetAllResto.php").build();
                //exécution de cette requête
                OkHttpClient httpclient = new OkHttpClient();
                httpclient.newCall(requestClients).enqueue(new Callback() {
                    //cette requête s’exécutera de façon asynchrone si la requête échoue affichage d'un message d'erreur dans les log
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                        Log.i("Erreur", "erreur requête getAllLacsJSON.php");
                    }

                    //si la requête réussie
                    public void onResponse(Call call, Response response) throws IOException {
                        //on va traiter la réponse
                        final String myResponse = response.body().string();
                        Log.d("Connexion", "La connexion à la base de donnée à été réussie");
                        activity_listeresto.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    // on crée un objet JSON à partir de notre réponse.
                                    JSONObject jsonObjectlesResto = new JSONObject(myResponse);
                                    //on transforme cet objet JSON en array d'objet lac sous forme JSON
                                    JSONArray jsonArray = jsonObjectlesResto.optJSONArray("resto");
                                    //on parcours cette collection d'objet lacs pour ajouter chaque lac dans notre liste d'objet lac
                                    lesResto.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        String idResto = jsonObject.getString("idR");
                                        String nomResto = jsonObject.getString("nomR");
                                        String numAdresseResto = jsonObject.getString("numAdrR");
                                        String voieAdresseResto = jsonObject.getString("voieAdrR");
                                        String codePostalResto = jsonObject.getString("cpR");
                                        String villeResto = jsonObject.getString("villeR");
                                        String latitudeResto = jsonObject.getString("latitudeDegR");
                                        String longitudeResto = jsonObject.getString("longitudeDegR");
                                        String descriptionResto = jsonObject.getString("descR");
                                        String horaireResto = jsonObject.getString("horairesR");
                                        ArrayList<Photo> lesPhotos = new ArrayList<Photo>();
                                        JSONArray jsonPhotos = jsonObject.getJSONArray("photos");
                                        for (int j = 0; j < jsonPhotos.length(); j++) {
                                            JSONObject jsonPhoto = jsonPhotos.getJSONObject(j);
                                            Photo photo = new Photo(jsonPhoto.getInt("idP"), jsonPhoto.get("cheminP").toString());
                                            lesPhotos.add(photo);
                                        }

                                        Resto unResto = new Resto(Integer.parseInt(idResto), nomResto, numAdresseResto, voieAdresseResto, codePostalResto, villeResto, Float.parseFloat(latitudeResto), Float.parseFloat(longitudeResto), descriptionResto, horaireResto);
                                        unResto.setLesPhotos(lesPhotos);
                                        //on ajoute le resto à la collection lesResto
                                        lesResto.add(unResto);
                                        ArrayAdapter<Resto> dataAdapter = new ArrayAdapter<Resto>(activity_listeresto.this, android.R.layout.simple_list_item_1, lesResto);
                                        listViewLesRestaurants.setAdapter(new RestoAdapter(activity_listeresto.this, lesResto));

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });


                    }

                });
            }
        });

        // -------------- FIN DU RECHARGEMENT DE LA LISTE SI LE BOUTON RELOAD EST PRESSE -----------------------

    }
}